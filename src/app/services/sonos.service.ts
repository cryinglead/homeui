import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Presents, Zone } from '../Interfaces/sonos';

@Injectable({
  providedIn: 'root'
})
export class SonosService {
  url = 'http://192.168.178.20:5005/'
  constructor(private http: HttpClient) { }

  getZones(): Observable<Zone[]> {
    return this.http.get<Zone[]>(`${this.url}zones`).pipe(tap(_ => this.log('getZones', _), 
      catchError(err => of(this.log('getZones failed', err)))));
  }

  play(room: string) {
    return this.http.get<Zone[]>(`${this.url}${room}/play`).pipe(tap(_ => this.log('play', _), 
      catchError(err => of(this.log('play failed', err)))));
  }

  stop(room: string) {
    return this.http.get<Zone[]>(`${this.url}${room}/pause`).pipe(tap(_ => this.log('stop', _), 
      catchError(err => of(this.log('stop failed', err)))));
  }

  mute(room: string) {
    return this.http.get<Zone[]>(`${this.url}${room}/mute`).pipe(tap(_ => this.log('mute', _), 
      catchError(err => of(this.log('mute failed', err)))));
  }

  unmute(room: string) {
    return this.http.get<Zone[]>(`${this.url}${room}/unmute`).pipe(tap(_ => this.log('unmute', _), 
      catchError(err => of(this.log('unmute failed', err)))));
  }

  next(room: string) {
    return this.http.get<Zone[]>(`${this.url}${room}/next`).pipe(tap(_ => this.log('next', _), 
      catchError(err => of(this.log('next failed', err)))));
  }

  previous(room: string) {
    return this.http.get<Zone[]>(`${this.url}${room}/previous`).pipe(tap(_ => this.log('previous', _), 
      catchError(err => of(this.log('previous failed', err)))));
  }

  volume(room: string, volume: string) {
    console.log('test');
    console.log(volume);
    return this.http.get<Zone[]>(`${this.url}${room}/volume/${volume}`).pipe(tap(_ => this.log('volume', _), 
      catchError(err => of(this.log('volume failed', err)))));
  }

  log(txt: string, object) {
    console.log(txt+': ');
    console.log(object);
  }
}
