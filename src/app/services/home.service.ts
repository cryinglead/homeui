import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private apiUrl = 'http://localhost:5000/api/'
  constructor(private http: HttpClient) { 

  }

  getTestData(): Observable<any> {
    return this.http.get(`${this.apiUrl}home`).pipe(
      tap(r => console.log(r))
      );
  }
}
