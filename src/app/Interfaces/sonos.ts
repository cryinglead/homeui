export interface Presents {
    players: Player[];
    playMode: PlayMode;
    pauseOthers: boolean;
    favorite: string;
}

export interface Player {
    roomName: string;
    volume: number;
}

export interface Zone {
  uuid: string;
  coordinator: Coordinator;
  members: Coordinator[];
}

export interface Coordinator {
  uuid: string;
  state: State;
  roomName: string;
  coordinator: string;
  groupState: GroupState;
}

export interface GroupState {
  volume: number;
  mute: boolean;
}

export interface State {
  volume: number;
  mute: boolean;
  equalizer: Equalizer;
  currentTrack: CurrentTrack;
  nextTrack: NextTrack;
  trackNo: number;
  elapsedTime: number;
  elapsedTimeFormatted: string;
  playbackState: string;
  playMode: PlayMode;
}

export interface PlayMode {
  repeat: string;
  shuffle: boolean;
  crossfade: boolean;
}

export interface NextTrack {
  artist: string;
  title: string;
  album: string;
  albumArtUri: string;
  duration: number;
  uri: string;
}

export interface CurrentTrack {
  artist: string;
  title: string;
  albumArtUri: string;
  duration: number;
  uri: string;
  trackUri: string;
  type: string;
  stationName: string;
  absoluteAlbumArtUri: string;
}

export interface Equalizer {
  bass: number;
  treble: number;
  loudness: boolean;
}