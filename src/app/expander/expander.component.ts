import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { expansionAnimations } from './expansion.animations';
import { coerceBooleanProperty} from '@angular/cdk/coercion'
import { Subject } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import {AnimationEvent} from '@angular/animations';
import {hasModifierKey} from '@angular/cdk/keycodes';

@Component({
  selector: 'expander',
  templateUrl: './expander.component.html',
  styleUrls: ['./expander.component.scss'],
  animations: [expansionAnimations.bodyExpansion, expansionAnimations.indicatorRotate]
})
export class ExpanderComponent implements OnInit {
  _bodyAnimationDone = new Subject<AnimationEvent>();
  @Output() afterExpand = new EventEmitter<void>();
  @Output() afterCollapse = new EventEmitter<void>();
  @Input() 
  get expanded() {
    return this._expanded;
  };
  set expanded(value) {
    this._expanded = coerceBooleanProperty(value);
  }
  _expanded = false;

  _keydown(event: KeyboardEvent) {
    switch (event.key) {
      // Toggle for space and enter keys.
      case 'Space':
      case 'Enter':
        if (!hasModifierKey(event)) {
          event.preventDefault();
          this.toggle();
        }

        break;
    }
  }


  @Input() 
  get hideToggle() {
    return this._hideToggle;
  };
  set hideToggle(value) {
    this._hideToggle = coerceBooleanProperty(value);
  }
  _hideToggle = false;


  constructor() { this._bodyAnimationDone.pipe(distinctUntilChanged((x, y) => {
    return x.fromState === y.fromState && x.toState === y.toState;
  })).subscribe(event => {
    if (event.fromState !== 'void') {
      if (event.toState === 'expanded') {
        this.afterExpand.emit();
      } else if (event.toState === 'collapsed') {
        this.afterCollapse.emit();
      }
    }
  });}

  _getExpandedState() {
    return this.expanded ? 'expanded' : 'collapsed';
  }
  
  ngOnInit() {
  }

  toggle(): void {
    this.expanded = !this.expanded;
  }

}
