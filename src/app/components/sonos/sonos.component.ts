import { Presents, Player, Zone } from './../../Interfaces/sonos';
import { Observable } from 'rxjs';
import { SonosService } from './../../services/sonos.service';
import { Component, OnInit } from '@angular/core';
import { faRedo } from '@fortawesome/free-solid-svg-icons';
import * as SonosActions from '../../store/Sonos/sonos.action';
import { ISonosState, sonosReducer } from 'src/app/store/Sonos/sonos.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-sonos',
  templateUrl: './sonos.component.html',
  styleUrls: ['./sonos.component.scss']
})
export class SonosComponent implements OnInit {
  faRedo = faRedo;
  constructor(private store$: Store<{sonosState: ISonosState}>) { }

  zones$: Observable<Zone[]>;

  ngOnInit() {
    this.zones$ = this.store$.select(x => x.sonosState.zones);
    this.load();
  }

  load() {
    this.store$.dispatch(SonosActions.loadZones());
  }

}