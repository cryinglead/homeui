import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SonosStateComponent } from './sonos-state.component';

describe('SonosStateComponent', () => {
  let component: SonosStateComponent;
  let fixture: ComponentFixture<SonosStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SonosStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SonosStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
