import { ISonosState } from '../../../store/Sonos/sonos.reducer';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { State } from 'src/app/Interfaces/sonos';
import { Store } from '@ngrx/store';
import * as SonosActions from '../../../store/Sonos/sonos.action';

@Component({
  selector: 'app-sonos-state',
  templateUrl: './sonos-state.component.html',
  styleUrls: ['./sonos-state.component.scss']
})
export class SonosStateComponent implements OnInit {

  @Input() state: State;
  @Input() room: string;
  play() {
    this.store$.dispatch(SonosActions.playRoom({room: this.room}));
  }

  stop() {
    this.store$.dispatch(SonosActions.stopRoom({room: this.room}));
  }

  previous() {
    this.store$.dispatch(SonosActions.previousRoom({room: this.room}));
  }

  next() {
    this.store$.dispatch(SonosActions.nextRoom({room: this.room}));
  }

  constructor(private store$: Store<{sonosStore: ISonosState}>) { }

  ngOnInit() {
  }
}
