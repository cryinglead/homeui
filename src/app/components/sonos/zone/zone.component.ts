import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Zone } from 'src/app/Interfaces/sonos';
import { ISonosState } from 'src/app/store/Sonos/sonos.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-zone',
  templateUrl: './zone.component.html',
  styleUrls: ['./zone.component.scss']
})
export class ZoneComponent implements OnInit {

  constructor(private store$: Store<ISonosState>) { }
  @Input() zone: Zone;
  @Output() reload = new EventEmitter();

  ngOnInit() {
  }
}