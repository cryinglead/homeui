import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SonosMemberComponent } from './sonos-member.component';

describe('SonosMemberComponent', () => {
  let component: SonosMemberComponent;
  let fixture: ComponentFixture<SonosMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SonosMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SonosMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
