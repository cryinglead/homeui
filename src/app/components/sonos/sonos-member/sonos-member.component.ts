import { ISonosState } from 'src/app/store/Sonos/sonos.reducer';
import { SonosService } from './../../../services/sonos.service';
import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Coordinator } from 'src/app/Interfaces/sonos';
import { faAngleRight, faAngleDown, faVolumeUp, faVolumeMute } from '@fortawesome/free-solid-svg-icons';
import { Store, select } from '@ngrx/store';
import { selectVolume } from 'src/app/store/Sonos/sonos.selectors';
import * as SonosActions from 'src/app/store/Sonos/sonos.action';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';
import * as $ from 'jquery';


@Component({
  selector: 'app-sonos-member',
  templateUrl: './sonos-member.component.html',
  styleUrls: ['./sonos-member.component.scss']
})
export class SonosMemberComponent implements OnInit {
  @ViewChild('volume') volumeArea: ElementRef<HTMLDivElement>;
  @ViewChild('slider') slider : ElementRef;
  @Input() member: Coordinator;
  @Input() isMainGroup: boolean;

  faAngleRight = faAngleRight;
  collapsed = false;
  faAngleDown = faAngleDown;
  faVolumeUp = faVolumeUp;
  faVolumeMute = faVolumeMute;
  lastChange = new Date();
  newVolume: string;
  exec;
  moment = extendMoment(Moment);
  ngOnInit(): void {
    $('.collapse').on('shown.bs.collapse', function() {
      document.getElementById('collapsedIcon').style.visibility = 'collapse';
      document.getElementById('showIcon').style.visibility = 'visible';
    }
    ).on('hidden.bs.collapse', function() {
      document.getElementById('collapsedIcon').style.visibility = 'visible';
      document.getElementById('showIcon').style.visibility = 'collapse';
  });}

  constructor(private sonosService: SonosService, private store$: Store<{sonos: ISonosState}>) { }

  setCollapsed() {
    this.collapsed = true;
  }1

  setVisible() {
    this.collapsed = false;
  }

  mute() {
    this.sonosService.mute(this.member.roomName);
  }

  unmute() {
      this.sonosService.unmute(this.member.roomName);
  }

  onValueChange(event: string) {
    console.log(this.collapsed);
    this.newVolume = event;
    clearTimeout(this.exec);
    this.exec = setTimeout(this.setVolume.bind(this), 500);
  }

  setVolume() {
    this.store$.dispatch(SonosActions.setVolume({volume: this.slider.nativeElement.value , room: this.member.roomName}));
  }
}
