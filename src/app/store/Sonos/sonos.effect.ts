import { playRoom, previousRoomFailure, setVolume } from './sonos.action';
import { SonosService } from '../../services/sonos.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError, switchMap, filter } from 'rxjs/operators';
import * as SonosActions from './sonos.action';
 
@Injectable()
export class SonosEffects {
 
  loadZones$ = createEffect(() => this.actions$.pipe(
    ofType(SonosActions.loadZones),
    mergeMap(() => this.sonosService.getZones()
      .pipe(
        map(zones => SonosActions.loadZonesSuccess({zones})),
        catchError(err => of(SonosActions.loadZonesFailure({error: err})))
      ))
    )
  );

  playRoom$ = createEffect(() => this.actions$.pipe(
    ofType(SonosActions.playRoom),
    mergeMap(({room}) => this.sonosService.play(room)
      .pipe(
        map(() => SonosActions.playRoomSuccess()),
        catchError(err => of(SonosActions.playRoomFailure({error: err})))
      ))
    )
  );

  pauseRoom$ = createEffect(() => this.actions$.pipe(
    ofType(SonosActions.stopRoom),
    mergeMap(({room}) => this.sonosService.stop(room)
      .pipe(
        map(() => SonosActions.stopRoomSuccess()),
        catchError(err => of(SonosActions.stopRoomFailure({error: err})))
      ))
    )
  );

  nextRoom$ = createEffect(() => this.actions$.pipe(
    ofType(SonosActions.nextRoom),
    mergeMap(({room}) => this.sonosService.next(room)
      .pipe(
        map(() => SonosActions.stopRoomSuccess()),
        catchError(err => of(SonosActions.stopRoomFailure({error: err})))
      ))
    )
  );

  previousRoom$ = createEffect(() => this.actions$.pipe(
    ofType(SonosActions.previousRoom),
    mergeMap(({room}) => this.sonosService.previous(room)
      .pipe(
        map(() => SonosActions.previousRoomSuccess()),
        catchError(err => of(SonosActions.previousRoomFailure({error: err})))
      ))
    )
  );

  setVolume$ = createEffect(() => this.actions$.pipe(
    ofType(SonosActions.setVolume),
    mergeMap((action: {room, volume}) => this.sonosService.volume(action.room, action.volume)
      .pipe(
        map(() => SonosActions.setVolumeSuccess()),
        catchError(err => of(SonosActions.setVolumeFailure({error: err})))
      ))
    )
  );

  reload$ = createEffect(() => this.actions$.pipe(
    filter((action) => action.type.includes('success') && action.type != SonosActions.loadZonesSuccess.type),
    map((action) => SonosActions.loadZones())
    )
  );
 
  constructor(
    private actions$: Actions,
    private sonosService: SonosService
  ) {}
}