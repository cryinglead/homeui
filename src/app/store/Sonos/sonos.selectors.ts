import { ISonosState } from "./sonos.reducer";
import { createSelector } from "@ngrx/store";
import { Zone } from "src/app/Interfaces/sonos";

export const selectZones = (state: ISonosState) => state.zones;
export const selectVolume = createSelector(
    selectZones,
    (zones: Zone[], props) => zones.find(x => x.members.find(y => y.roomName === props.roomName)).members.find(x => x.roomName === props.roomName).state.volume
);