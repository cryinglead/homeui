import { createAction, props } from "@ngrx/store";

export const loadZones = createAction('[Sonos] Load zones');
export const loadZonesSuccess = createAction('[Sonos] Load zones success', props<{zones}>());
export const loadZonesFailure = createAction('[Sonos] Load zones failure', props<{error}>());

export const playRoom = createAction('[Sonos] play zone', props<{room}>());
export const playRoomSuccess = createAction('[Sonos] play zone success');
export const playRoomFailure = createAction('[Sonos] play zone failure', props<{error}>());

export const stopRoom = createAction('[Sonos] stop zone', props<{room}>());
export const stopRoomSuccess = createAction('[Sonos] stop zone success');
export const stopRoomFailure = createAction('[Sonos] stop zone failure', props<{error}>());

export const previousRoom = createAction('[Sonos] previous zone', props<{room}>());
export const previousRoomSuccess = createAction('[Sonos] previous zone success');
export const previousRoomFailure = createAction('[Sonos] previous zone failure', props<{error}>());

export const nextRoom = createAction('[Sonos] next zone', props<{room}>());
export const nextRoomSuccess = createAction('[Sonos] next zone success');
export const nextRoomFailure = createAction('[Sonos] next zone failure', props<{error}>());

export const setVolume = createAction('[Sonos] set volume', props<{room, volume}>());
export const setVolumeSuccess = createAction('[Sonos] set volume success');
export const setVolumeFailure = createAction('[Sonos] set volume failure', props<{error}>());