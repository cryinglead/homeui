import { Zone } from '../../Interfaces/sonos';
import * as SonosActions from './sonos.action';
import { Action, createReducer, on, MetaReducer, ActionReducer } from '@ngrx/store';

export interface ISonosState {
    loading: boolean;
    error: string;
    zones: Zone[];
}

export const initialSonosState: ISonosState = {
    loading: false,
    error: undefined,
    zones: []
}

const _sonosReducer = createReducer(
    initialSonosState,
    on(SonosActions.loadZones, state => ({ ...state, loading: true })),
    on(SonosActions.loadZonesSuccess, (state, {zones}) => ({ ...state, loading: false, zones: zones })),
    on(SonosActions.loadZonesFailure, (state, {error}) => ({ ...state, loading: false, error: error })),

    on(SonosActions.playRoom, state => ({ ...state, loading: true })),
    on(SonosActions.playRoomSuccess, (state) => ({ ...state, loading: false })),
    on(SonosActions.playRoomFailure, (state, { error }) => ({ ...state, loading: false, error: error })),

    on(SonosActions.stopRoom, state => ({ ...state, loading: true })),
    on(SonosActions.stopRoomSuccess, (state) => ({ ...state, loading: false })),
    on(SonosActions.stopRoomFailure, (state, { error }) => ({ ...state, loading: false, error: error })),

    on(SonosActions.nextRoom, state => ({ ...state, loading: true })),
    on(SonosActions.nextRoomSuccess, (state) => ({ ...state, loading: false })),
    on(SonosActions.nextRoomFailure, (state, { error }) => ({ ...state, loading: false, error: error })),

    on(SonosActions.previousRoom, state => ({ ...state, loading: true })),
    on(SonosActions.previousRoomSuccess, (state) => ({ ...state, loading: false })),
    on(SonosActions.previousRoomFailure, (state, { error }) => ({ ...state, loading: false, error: error })),

    on(SonosActions.setVolume, state => ({ ...state, loading: true })),
    on(SonosActions.setVolumeSuccess, (state) => ({ ...state, loading: false })),
    on(SonosActions.setVolumeFailure, (state, { error }) => ({ ...state, loading: false, error: error })),
  );
  
  export function sonosReducer(state: ISonosState | undefined, action: Action) {
    return _sonosReducer(state, action);
  }

  // console.log all actions
  export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
    return function(state, action) {
      console.log('state', state);
      console.log('action', action);
  
      return reducer(state, action);
    };
  }
  export const metaReducers: MetaReducer<any>[] = [debug];