import { ISonosState, initialSonosState, sonosReducer } from 'src/app/store/Sonos/sonos.reducer';
import { ActionReducerMap } from "@ngrx/store";

export interface IAppState {
    sonosState: ISonosState;
}

export const initialAppState: IAppState = {
    sonosState: initialSonosState,
}

export const reducers: ActionReducerMap<IAppState> = {
    sonosState: sonosReducer,
};