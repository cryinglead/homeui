import { SonosEffects } from './store/Sonos/sonos.effect';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { SonosComponent } from './components/sonos/sonos.component';
import { StoreModule } from '@ngrx/store';
import { ZoneComponent } from './components/sonos/zone/zone.component';
import { SonosMemberComponent } from './components/sonos/sonos-member/sonos-member.component';
import { SonosStateComponent } from './components/sonos/sonos-state/sonos-state.component';
import { EffectsModule } from '@ngrx/effects';
import { metaReducers } from './store/Sonos/sonos.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { reducers } from './store/reducers/app.reducers';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { IconComponent } from './icon/icon.component';
import { IconButtonComponent } from './icon-button/icon-button.component';
import { ExpanderComponent } from './expander/expander.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReceiptsComponent } from './receipts/receipts.component';

const routes: Routes = [
  { path: '', redirectTo: '/sonos', pathMatch: 'full' },
  { path: 'sonos', component: SonosComponent },
  { path: 'receipts', component: ReceiptsComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    SonosComponent,
    ZoneComponent,
    SonosMemberComponent,
    SonosStateComponent,
    IconComponent,
    IconButtonComponent,
    ExpanderComponent,
    ReceiptsComponent
  ],  
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    OAuthModule.forRoot(),
    RouterModule.forRoot(routes),
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    EffectsModule.forRoot([SonosEffects]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),DragDropModule,BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
